void sortBySelectionInt(int *array, int n){
    for (int i = 0; i < n; i++) {
        int min = array[i];
        int index = i;
        for (int j = i; j < n; j++) {
            if (min > array[j]) {
                array[i] = array[j];
                array[j] = min;
                min = array[i];
            }
        }
    }
}
